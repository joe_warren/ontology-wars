module Main exposing (..)

import Browser
import Html exposing (Html, text, div, h1, img)
import Html.Attributes exposing (src)
import Json.Decode exposing (Decoder, field, string, map2)
import Http

---- MODEL ----

type alias Category =
  { name : String
  , url : String
  }

categoryDecoder : Decoder Category
categoryDecoder =
  map2 Category
      (field "classname" string)
      (field "classUrl" string)

type Model =
    Loading |
    Failure |
    Success Category


init : ( Model, Cmd Msg )
init =
    ( Loading, getCurrency )



---- UPDATE ----


type Msg
    = NoOp |
      GotCategory (Result Http.Error Category)

getCurrency : Cmd Msg
getCurrency =
  Http.get
    { url = "/Currency.json"
    , expect = Http.expectJson GotCategory categoryDecoder
    }

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model = case msg of
    NoOp -> ( model, Cmd.none )
    GotCategory result -> case result of
        Ok cat -> (Success cat, Cmd.none)
        Err _ -> (Failure, Cmd.none)



---- VIEW ----


view : Model -> Html Msg
view model = case model of
    Loading -> div []
        [ img [ src "/logo.svg" ] []
        , h1 [] [ text "Your Elm App is loading!" ]
        ]
    Failure -> div []
        [ img [ src "/logo.svg" ] []
        , h1 [] [ text "Your Elm App has Failed!" ]
       ]
    Success category -> div []
        [ img [ src "/logo.svg" ] []
        , h1 [] [ text (category.name) ]
        ]


---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }
