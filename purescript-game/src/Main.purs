module Main where

import Prelude
import App.Events (AppEffects, Event(..), foldp)
import App.Routes (match)
import App.State (State, init)
import App.View.Layout (view)
import Control.Monad.Eff (Eff)
import DOM (DOM)
import DOM.HTML (window)
import DOM.HTML.Types (HISTORY)
import Pux (CoreEffects, App, start)
import Pux.DOM.Events (DOMEvent)
import Pux.DOM.History (sampleURL)
import Pux.Renderer.React (renderToDOM)
import Signal ((~>))

import Data.List (List (..), length, (!!))
import Partial.Unsafe (unsafePartial)
import Control.Monad.Eff.Random as Rand
import Network.HTTP.Affjax as AX
import Data.Argonaut.Core as J
import Data.Argonaut.Decode as JD
import Data.Argonaut.Decode.Combinators ((.?))
import Data.Either (Either(..))
import Data.Maybe (Maybe(..), fromJust)
import Data.HTTP.Method (Method(..))
import Control.Monad.Eff.Console (log, CONSOLE)
import Control.Monad.Eff.Class (liftEff)
import Control.Monad.Aff (Aff, launchAff)
type WebApp = App (DOMEvent -> Event) Event State

type ClientEffects = CoreEffects (AppEffects (history :: HISTORY, dom :: DOM, ajax :: AX.AJAX, console :: CONSOLE))

newtype Instance = Instance
  { name :: String
  , url :: String
  }

newtype Category = Category
  { classname :: String
  , classUrl :: String
  , instances :: List Instance
  }

-- create a `DecodeJson` instance
instance decodeJsonInstance :: JD.DecodeJson Instance where
  decodeJson json = do
    x <- JD.decodeJson json
    n <- x .? "name" 
    u <- x .? "url" 
    pure $ Instance { name: n, url: u}


instance decodeJsonCategory :: JD.DecodeJson Category where
  decodeJson json = do
    x <- JD.decodeJson json
    n <- x .? "classname" 
    u <- x .? "classUrl"
    i <- x .? "instances"
    pure $ Category {classname: n, classUrl: u, instances: i}


randomChoice :: forall a e. List a -> Eff (random :: Rand.RANDOM | e) a
randomChoice l = do  
    i <- Rand.randomInt 0 ((length l) -1)
    pure $ unsafePartial $ fromJust (l !! i)

type LoadingEffects = CoreEffects (AppEffects (ajax :: AX.AJAX, console :: CONSOLE, random :: Rand.RANDOM))
loadRandomString :: String -> Aff LoadingEffects (Maybe String)
loadRandomString url = do
    res <- AX.affjax $ AX.defaultRequest { url = url, method = Left GET }
    case JD.decodeJson res.response of
        Left error -> do 
            liftEff $ log $ "Error: " <> error
            pure Nothing
        Right (list) -> Just <$> (liftEff $ randomChoice list)
            

main :: String -> State -> Eff ClientEffects WebApp
main url state = do
  -- | Create a signal of URL changes.
  urlSignal <- sampleURL =<< window

  -- | Map a signal of URL changes to PageView actions.
  let routeSignal = urlSignal ~> \r -> PageView (match r)
  _ <- launchAff $ do
    res <- AX.affjax $ AX.defaultRequest { url = "/Currency.json", method = Left GET }
    case JD.decodeJson res.response of
        Left error -> liftEff $ log $ "Error: " <> error
        Right (Category {classname: n}) -> liftEff $ log $ "Category: " <> (n)

  -- | Start the app.
  app <- start
    { initialState: state
    , view
    , foldp
    , inputs: [routeSignal] }

  -- | Render to the DOM
  renderToDOM "#app" app.markup app.input

  -- | Return app to be used for hot reloading logic in support/client.entry.js
  pure app

initialState :: State
initialState = init "/"
