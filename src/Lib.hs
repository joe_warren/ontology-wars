{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module Lib
    ( someFunc
    ) where

import GHC.Generics

import Database.HSparql.Connection
import Database.HSparql.QueryGenerator

import Data.RDF hiding (triple, Query)
import qualified Data.Text as T
import qualified Data.Text.IO as T

import Control.Monad

import Data.Aeson

data Instance = Instance {
        url :: T.Text, 
        name :: T.Text
    } deriving (Generic, Show)

data Category = Category {
        classUrl :: T.Text, 
        classname :: T.Text, 
        instances :: [Instance]
    } deriving (Generic, Show)

instance ToJSON Instance where
    toEncoding = genericToEncoding defaultOptions

instance ToJSON Category where
    toEncoding = genericToEncoding defaultOptions

fromBindingValuesInstance :: [BindingValue] -> Maybe Instance
fromBindingValuesInstance [name, url] = Just $ Instance (printBinding url) (printBinding name)
fromBindingValuesInstance _ = Nothing

fromBindingValues :: BindingValue -> BindingValue -> [[BindingValue]] -> Maybe Category
fromBindingValues classname classurl instances = Category (printBinding classurl) (printBinding classname) <$> traverse fromBindingValuesInstance instances

saveCategory :: String -> Category -> IO ()
saveCategory filename cat = encodeFile (filename ++ ".json") cat

endpoint :: String
endpoint = "http://dbpedia.org/sparql"

subtypesOf :: T.Text -> T.Text -> Query SelectQuery
subtypesOf thePrefix root = do
    pre <- prefix "pre" (iriRef thePrefix)
    xsd  <- prefix "xsd" (iriRef "http://www.w3.org/2001/XMLSchema#")
    prop <- prefix "prop" (iriRef "http://dbpedia.org/property/")
    dbo  <- prefix "dbo" (iriRef "http://dbpedia.org/ontology/")
    foaf <- prefix "foaf" (iriRef "http://xmlns.com/foaf/0.1/")
    resc <- prefix "resc" (iriRef "http://dbpedia.org/resource/")
    yago <- prefix "yago" (iriRef "http://dbpedia.org/class/yago/")

    rdf <- prefix "rdf"  (iriRef "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
    rdfs <- prefix "rdfs"  (iriRef "http://www.w3.org/2000/01/rdf-schema#")
    owl <- prefix "owl"  (iriRef "http://www.w3.org/2002/07/owl#")

    clazz <- var
    classname <- var

    triple clazz (rdfs .:. "label") classname
    triple clazz (rdfs  .:. "subClassOf") (pre .:. root)

    filterExpr $ langMatches (lang classname) ("en" :: T.Text)

    distinct
    limit 1000

    return SelectQuery { queryExpr = SelectVar <$> [classname, clazz] }


printBinding :: BindingValue -> T.Text
printBinding (Bound (LNode (PlainLL name _))) = name
printBinding (Bound (UNode url)) = url
printBinding _ = "..."

instancesOf :: T.Text -> T.Text -> Query SelectQuery
instancesOf thePrefix clazz = do
    pre <- prefix "pre" (iriRef thePrefix)
    xsd  <- prefix "xsd" (iriRef "http://www.w3.org/2001/XMLSchema#")
    prop <- prefix "prop" (iriRef "http://dbpedia.org/property/")
    dbo  <- prefix "dbo" (iriRef "http://dbpedia.org/ontology/")
    foaf <- prefix "foaf" (iriRef "http://xmlns.com/foaf/0.1/")
    resc <- prefix "resc" (iriRef "http://dbpedia.org/resource/")
    yago <- prefix "yago" (iriRef "http://dbpedia.org/class/yago/")

    rdf <- prefix "rdf"  (iriRef "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
    rdfs <- prefix "rdfs"  (iriRef "http://www.w3.org/2000/01/rdf-schema#")
    owl <- prefix "owl"  (iriRef "http://www.w3.org/2002/07/owl#")

    obj <- var
    name <- var

    triple obj (foaf .:. "name") name
    triple obj (rdf  .:. "type") (pre .:. clazz)

    distinct
    limit 1000

    return SelectQuery { queryExpr = SelectVar <$> [name, obj] }

splitPrefix :: T.Text -> (T.Text, T.Text)
splitPrefix x = case T.split (== '#') x  of
                  [noanchor] -> let c = T.split (== '/') noanchor in
                                    (T.concat $ (flip T.snoc $ '/') <$> (init c) , last c)
                  withanchor -> (T.concat $ (flip T.snoc $ '#') <$> (init withanchor), last withanchor)

printInstances :: [BindingValue] -> IO ()
printInstances [classname, clazz] = do
    let clazzUrl = printBinding clazz
    let (thePrefix, classStripped) = splitPrefix clazzUrl
    case classStripped of
        "Place" -> return () -- this stops us from recursing down places when looking for things
        "Person" -> return () -- it's probably best to avoid this rabit hole
        _ -> recurse thePrefix classStripped
    let q = instancesOf thePrefix classStripped
    res <- selectQuery endpoint q
    case res of
        Nothing -> putStrLn "error with DB connection"
        Just r -> do
            putStrLn ""
            T.putStrLn $ printBinding classname
            void $ (traverse.traverse) (T.putStrLn . printBinding) r
            
            case (length r) >= 6 of
                True -> case fromBindingValues classname clazz r of
                            Just cat -> saveCategory (T.unpack classStripped) cat
                            Nothing -> putStrLn "error converting binding values"
                False -> putStrLn "insufficient instances"

printInstances _ = putStrLn "error of some kind"



recurse :: T.Text -> T.Text -> IO ()
recurse thePrefix root = do
    let q = subtypesOf thePrefix root
    res <- selectQuery endpoint q
    case res of
        Nothing -> putStrLn "error with DB connection"
        Just r -> void $ traverse printInstances r


someFunc :: IO ()
-- someFunc = recurse "http://dbpedia.org/ontology/" "Place"
--someFunc = recurse "http://www.w3.org/2002/07/owl#" "Thing"
someFunc = printInstances [(Bound (LNode (PlainLL "Currency" "en"))), (Bound (UNode "http://dbpedia.org/ontology/Currency"))]
